## SQAaaS results :bellhop_bell:

### Quality criteria summary
| Result | Assertion | Subcriterion ID | Criterion ID |
| ------ | --------- | --------------- | ------------ |
| :heavy_check_mark: | Source code uses Git for version control | QC.Acc01 | QC.Acc |
| :heavy_check_mark: | A README file is present in the code repository | QC.Doc06.1 | QC.Doc |
| :heavy_multiplication_x: | A CODE_OF_CONDUCT file is not present in the code repository | QC.Doc06.3 | QC.Doc |
| :heavy_multiplication_x: | A CONTRIBUTING file is not present in the code repository | QC.Doc06.2 | QC.Doc |
| :heavy_check_mark: | Documentation resides in the same repository as code | QC.Doc01.1 | QC.Doc |
| :heavy_multiplication_x: | Docs are not fully compliant with markdownlint standard | QC.Doc02.X | QC.Doc |
| :heavy_multiplication_x: | C++ files are not fully compliant with oclint standard | QC.Sty01 | QC.Sty |
| :heavy_check_mark: | The code repository uses tags for releasing new software versions | QC.Ver01.0 | QC.Ver |
| :heavy_multiplication_x: | Latest release tag v1.0.0 found, but is not SemVer compliant | QC.Ver01 | QC.Ver |
| :heavy_multiplication_x: | Not all release tags are SemVer compliant | QC.Ver02 | QC.Ver |
| :heavy_multiplication_x: | No matching files found for language _CodeMeta_ in repository searching by extensions or filenames<br />No matching files found for language _Citation File Format_ in repository searching by extensions or filenames | QC.Met01 | QC.Met |
| :heavy_multiplication_x: | No matching files found for language _Python_ in repository searching by extensions or filenames<br />No matching files found for language _Go_ in repository searching by extensions or filenames | QC.Sec02 | QC.Sec |
| :heavy_multiplication_x: | No matching files found for language _Python_ in repository searching by extensions or filenames | QC.Uni01 | QC.Uni |
| :heavy_multiplication_x: | Error raised when validating tool _licensee_ with validator plugin _licensee_: argument should be a str or an os.PathLike object where __fspath__ returns a str, not 'NoneType' | QC.Lic01 | QC.Lic |
| :heavy_multiplication_x: | Error raised when validating tool _licensee_ with validator plugin _licensee_: argument should be a str or an os.PathLike object where __fspath__ returns a str, not 'NoneType' | QC.Lic01.1 | QC.Lic |
| :heavy_multiplication_x: | Error raised when validating tool _licensee_ with validator plugin _licensee_: argument should be a str or an os.PathLike object where __fspath__ returns a str, not 'NoneType' | QC.Lic02 | QC.Lic |
| :heavy_multiplication_x: | Error raised when validating tool _licensee_ with validator plugin _licensee_: argument should be a str or an os.PathLike object where __fspath__ returns a str, not 'NoneType' | QC.Lic02.1 | QC.Lic |

### Quality badge
shields.io-based badge: [![SQAaaS badge shields.io](https://github.com/EOSC-synergy/LandSlideHySEA.assess.sqaaas/raw/v1.0.0/.badge/status_shields.svg)](https://sqaaas.eosc-synergy.eu/#/full-assessment/report/https://raw.githubusercontent.com/eosc-synergy/LandSlideHySEA.assess.sqaaas/v1.0.0/.report/assessment_output.json)
 - Missing quality criteria for next level badge (bronze): [`QC.Lic`](https://indigo-dc.github.io/sqa-baseline/#licensing-qc.lic) [`QC.Doc`](https://indigo-dc.github.io/sqa-baseline/#documentation-qc.doc) 

### :clipboard: __View full report in the [SQAaaS platform](https://sqaaas.eosc-synergy.eu/#/full-assessment/report/https://raw.githubusercontent.com/eosc-synergy/LandSlideHySEA.assess.sqaaas/v1.0.0/.report/assessment_output.json)__