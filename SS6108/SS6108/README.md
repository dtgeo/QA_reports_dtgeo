## SQAaaS results :bellhop_bell:

### Quality criteria summary
| Result | Assertion | Subcriterion ID | Criterion ID |
| ------ | --------- | --------------- | ------------ |
| :heavy_multiplication_x: | No matching files found for language _CodeMeta_ in repository searching by extensions or filenames<br />No matching files found for language _Citation File Format_ in repository searching by extensions or filenames | QC.Met01 | QC.Met |
| :heavy_multiplication_x: | No matching files found for language _Python_ in repository searching by extensions or filenames<br />No matching files found for language _Go_ in repository searching by extensions or filenames | QC.Sec02 | QC.Sec |
| :heavy_multiplication_x: | No matching files found for language _Dockerfile_ in repository searching by extensions or filenames<br />No matching files found for language _JSON_ in repository searching by extensions or filenames<br />No matching files found for language _Python_ in repository searching by extensions or filenames<br />No matching files found for language _Go_ in repository searching by extensions or filenames<br />No matching files found for language _Ruby_ in repository searching by extensions or filenames<br />No matching files found for language _Java_ in repository searching by extensions or filenames | QC.Sty01 | QC.Sty |
| :heavy_multiplication_x: | No matching files found for language _Python_ in repository searching by extensions or filenames | QC.Uni01 | QC.Uni |

### Quality badge
shields.io-based badge: [![SQAaaS badge shields.io](https://github.com/EOSC-synergy/LandSlideHySEA.assess.sqaaas/raw/v1.0.0/.badge/status_shields.svg)](https://sqaaas.eosc-synergy.eu/#/full-assessment/report/https://raw.githubusercontent.com/eosc-synergy/LandSlideHySEA.assess.sqaaas/v1.0.0/.report/assessment_output.json)
 - Missing quality criteria for next level badge (bronze): [`QC.Acc`](https://indigo-dc.github.io/sqa-baseline/#code-accessibility-qc.acc) [`QC.Lic`](https://indigo-dc.github.io/sqa-baseline/#licensing-qc.lic) [`QC.Doc`](https://indigo-dc.github.io/sqa-baseline/#documentation-qc.doc) 

### :clipboard: __View full report in the [SQAaaS platform](https://sqaaas.eosc-synergy.eu/#/full-assessment/report/https://raw.githubusercontent.com/eosc-synergy/LandSlideHySEA.assess.sqaaas/v1.0.0/.report/assessment_output.json)__